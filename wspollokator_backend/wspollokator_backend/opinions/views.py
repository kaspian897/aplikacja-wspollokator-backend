from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema

from .models import Opinion
from .serializers import OpinionSerializer


class OpinionViewSet(viewsets.ModelViewSet):
    queryset = Opinion.objects.all()
    serializer_class = OpinionSerializer
    permission_classes = [IsAuthenticated]

    @action(
        detail=False,
        url_path=r"about/(?P<user_id>[^/.]+)",
        methods=["GET"],
    )
    @swagger_auto_schema(
        responses={200: OpinionSerializer(many=True)},
    )
    def about(self, request, user_id, *args, **kwargs):
        """Lists all opinions about user."""
        opinions_about_user = Opinion.objects.filter(about=user_id)
        serializer = OpinionSerializer(opinions_about_user, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    @action(
        detail=False,
        url_path=r"issued_by/(?P<user_id>[^/.]+)",
        methods=["GET"],
    )
    @swagger_auto_schema(
        responses={200: OpinionSerializer(many=True)},
    )
    def issued_by(self, request, user_id, *args, **kwargs):
        """Lists all opinions issued by user."""
        opinions_issued_by_user = Opinion.objects.filter(issued_by=user_id)
        serializer = OpinionSerializer(opinions_issued_by_user, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

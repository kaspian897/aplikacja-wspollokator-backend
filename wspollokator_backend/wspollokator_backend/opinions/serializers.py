from django.db.models import fields
from rest_framework import serializers

from .models import Opinion


class OpinionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Opinion
        fields = (
            "id",
            "issued_by",
            "about",
            "rate",
            "text",
            "created_at",
        )
        
    def validate(self, data):
        if data['issued_by'] == data['about']:
            raise serializers.ValidationError("Nie można wystawić opini o samym sobie.")
        return data
import uuid

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _


User = get_user_model()

class Opinion(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    issued_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,
                                  verbose_name=_("wystawiona przez"),
                                  related_name="issued_by"
    )
    about = models.ForeignKey(User, on_delete=models.DO_NOTHING,
                              verbose_name=_("o"),
                              related_name="about"
    )
    rate = models.PositiveIntegerField(default=1, verbose_name=_("ocena"),
        validators = [
            MinValueValidator(1),
            MaxValueValidator(5)
        ]
    )
    text = models.TextField(verbose_name="treść")
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_("dodana")
    )
from django.shortcuts import get_object_or_404
from rest_framework import mixins, viewsets, status
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from . import serializers
from .models import Favourites, Profile, Point, User
from rest_framework.response import Response
import django_filters
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import GeometryDistance
from django.db.models import OuterRef, Subquery
from django.contrib.gis.geos import GEOSGeometry
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from django.views.generic.detail import DetailView
from .serializers import NewFavouriteSerializer
from wspollokator_backend.conversation.models import Conversation


class ProfileFilter(django_filters.FilterSet):
    min_age = django_filters.NumberFilter(field_name="age", lookup_expr="gte")
    max_age = django_filters.NumberFilter(field_name="age", lookup_expr="lte")

    min_preferable_price = django_filters.NumberFilter(
        field_name="preferable_price", lookup_expr="gte"
    )
    max_preferable_price = django_filters.NumberFilter(
        field_name="preferable_price", lookup_expr="lte"
    )

    accepts_animals = django_filters.CharFilter(
        field_name="accepts_animals", method="filter_accepts_animals"
    )

    def filter_accepts_animals(self, queryset, name, value):

        if self.data["accepts_animals"] == "I":
            return queryset.filter()
        return queryset.filter(accepts_animals__in=[self.data["accepts_animals"], "I"])

    smoking = django_filters.CharFilter(field_name="smoking", method="filter_smoking")

    def filter_smoking(self, queryset, name, value):
        if self.data["smoking"] == "I":
            return queryset.filter()
        return queryset.filter(smoking__in=[self.data["smoking"], "I"])

    radius = django_filters.NumberFilter(field_name="distance", method="filter_radius")

    def filter_radius(self, queryset, name, value):
        return queryset.filter(location__distance_lte=(Point.objects.filter(user=self.request.user).first().location,D(km=self.data["radius"])))
    
    min_updated_at = django_filters.IsoDateTimeFilter(
        field_name="updated_at", lookup_expr="gte"
    )
	
    class Meta:
        model = Profile
        fields = [
            "sex",
            "accepts_animals",
            "min_age",
            "max_age",
            "smoking",
            "min_preferable_price",
            "max_preferable_price",
            "radius",
            "min_updated_at"
        ]


class SignupViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.SignupSerializer


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = serializers.ProfileSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = "user"
    parser_classes = (FormParser, MultiPartParser)

    filter_backends = (
        DjangoFilterBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    filterset_class = ProfileFilter

    search_fields = ["user__first_name", "user__last_name"]
    ordering_fields = ["preferable_price", "age", "distance","updated_at"]

    def list(self, request):
        """
        point -- punkt
        """
        point = self.request.query_params.get("point")
        user = self.request.user
        pnt = Point.objects.filter(user=user).first()
        f = True
        if not pnt:
            f = False
        if point:
            pnt = Point()
            pnt.location = GEOSGeometry(point)
            pnt.radius = 0
            f = True


        if not f:
            return Response([])
        points = Point.objects.filter(user=OuterRef("user"))
        subqueryLocation = Subquery(points.values("location")[:1])
        subqueryRadius = Subquery(points.values("radius")[:1])
        queryset = (
            Profile.objects.annotate(radius=subqueryRadius)
            .annotate(location=subqueryLocation)
            .annotate(distance=GeometryDistance(subqueryLocation, pnt.location))
            .filter(is_searchable=True, distance__isnull=False)
            .exclude(user=user)
        )

        if(self.request.query_params.get("radius")==None):
            queryset=queryset.filter(location__distance_lte=(Point.objects.filter(user=user).first().location,D(km=pnt.radius)))
        queryset = self.filter_queryset(queryset)

        serializer = serializers.FullProfileListSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)


class ProfileDetailView(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = serializers.ProfileDetailSerializer
    queryset = Profile.objects.all()
    permission_classes = [IsAuthenticated]


class PointViewSet(viewsets.ModelViewSet):
    queryset = Point.objects.all()
    serializer_class = serializers.PointSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """
        This view should return a list of all the points
        for the currently authenticated user.
        """
        user = self.request.user
        return Point.objects.filter(user=user)


class FavouritesViewSet(viewsets.ViewSet):

    permission_classes = [IsAuthenticated]

    @action(
        detail=False,
        url_path="add",
        url_name="add",
        methods=["POST"],
    )
    @swagger_auto_schema(
        request_body=NewFavouriteSerializer(),
        responses={201: "{user} has been added to your favourites."},
    )
    def add(self, request):
        """Adds user to favourites."""
        serializer = NewFavouriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.filter(pk=request.data["user_id"])
        if not user:
            return Response(
                "User with this id has not been found.",
                status=status.HTTP_404_NOT_FOUND,
            )
        user = user[0]
        fav = Favourites.objects.filter(user=request.user).filter(favourite=user.id)
        if fav:
            return Response(
                "You have this user in favourites already.",
                status=status.HTTP_400_BAD_REQUEST,
            )
        favourite = Favourites(user=request.user, favourite=user.id)
        favourite.save()
        return Response(
            f"{user} has been added to your favourites.", status=status.HTTP_201_CREATED
        )

    @action(
        detail=False,
        url_path="remove",
        url_name="remove",
        methods=["POST"],
    )
    @swagger_auto_schema(
        request_body=NewFavouriteSerializer(),
        responses={204: "Successfully removed from your favourites."},
    )
    def remove(self, request):
        """Removes user to favourites."""
        serializer = NewFavouriteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        favourite = get_object_or_404(
            Favourites, user=request.user, favourite=request.data["user_id"]
        )
        favourite.delete()
        return Response(
            f"Successfully removed from your favourites.",
            status=status.HTTP_204_NO_CONTENT,
        )

    @action(
        detail=False,
        url_path="list",
        url_name="list",
        methods=["GET"],
    )
    @swagger_auto_schema(
        responses={200: serializers.ProfileDetailSerializer(many=True)},
    )
    def lists(self, request):
        """Lists favourites profiles."""
        favourites = Favourites.objects.filter(user=request.user)
        response = []
        for object in favourites:
            user = User.objects.get(id=object.favourite)
            serializer = serializers.ProfileDetailSerializer(user.profile,context={"request": request})
            conversation = Conversation.objects.filter(users=request.user).filter(
                users=user
            )
            data = serializer.data
            if conversation:
                data["conversation_id"] = conversation[0].id
            response.append(data)
        return Response(response, status=status.HTTP_200_OK)

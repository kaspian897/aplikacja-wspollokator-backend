from django.contrib import admin

from .models import Favourites, User, Profile, Point

admin.site.register(User)
admin.site.register(Profile)
admin.site.register(Point)
admin.site.register(Favourites)

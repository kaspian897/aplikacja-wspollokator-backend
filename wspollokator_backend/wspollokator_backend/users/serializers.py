from django.contrib.auth.password_validation import validate_password
from django.db import models
from django.db.models import query
from rest_framework import serializers
from django.contrib.auth import get_user_model
from wspollokator_backend.conversation.models import Conversation
from .models import Profile, Point

User = get_user_model()


class NewFavouriteSerializer(serializers.Serializer):
    user_id = serializers.UUIDField()


class SignupSerializer(serializers.ModelSerializer):
    model = get_user_model()

    class Meta:
        model = get_user_model()
        fields = ["first_name", "last_name", "email", "password", "profile"]
        read_only_fields = ["profile"]
        optional_fields = ["profile"]
        extra_kwargs = {"password": {"write_only": True}}

    def validate_password(self, value):
        validate_password(value)
        return value

    def create(self, validated_data):
        user = User(**validated_data)
        user.set_password(validated_data["password"])
        user.save()
        return user


class PointSerializer(serializers.ModelSerializer):
    class Meta:
        model = Point
        fields = ["id", "location", "radius", "user"]
        read_only_fields = ["user"]
        optional_fields = ["user"]

    def create(self, validated_data):
        request = self.context.get("request")
        user = request.user
        point = Point.objects.create(user=user, **validated_data)
        return point


class ProfileSerializer(serializers.ModelSerializer):
    avatar = serializers.FileField(required=False, allow_null=True)

    class Meta:
        model = Profile
        fields = [
            "id",
            "updated_at",
            "sex",
            "age",
            "accepts_animals",
            "smoking",
            "preferable_price",
            "description",
            "is_searchable",
            "avatar",
        ]

    def create(self, validated_data):
        request = self.context.get("request")
        user = request.user
        profile = Profile.objects.create(user=user, **validated_data)
        return profile


class UserSerializer(serializers.ModelSerializer):
    model = get_user_model()

    class Meta:
        model = get_user_model()
        fields = ["id", "first_name", "last_name", "email", "profile"]


class FullProfileListSerializer(serializers.ModelSerializer):
    avatar = serializers.FileField(required=False, allow_null=True)
    user = UserSerializer(read_only=True)

    point = serializers.SerializerMethodField("get_point")

    distance = serializers.SerializerMethodField()

    def get_distance(self, obj):
        try:
            return obj.distance
        except:
            return None

    def get_point(self, obj):
        return PointSerializer(
            Point.objects.all().filter(user=obj.user), many=True
        ).data

    class Meta:
        model = Profile
        fields = [
            "id",
            "updated_at",
            "sex",
            "age",
            "accepts_animals",
            "smoking",
            "preferable_price",
            "description",
            "is_searchable",
            "avatar",
            "user",
            "point",
            "distance",
        ]


class UserNamesSerialiazer(serializers.ModelSerializer):
    class Meta:
        model = User
        field = ["first_name", "last_name"]


class ProfileDetailSerializer(serializers.ModelSerializer):
    avatar = serializers.FileField(required=False, allow_null=True)
    user_id = serializers.CharField(source="user.id")
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    conversation_id = serializers.SerializerMethodField()
    point = serializers.SerializerMethodField("get_point")

    class Meta:
        model = Profile
        fields = [
            "id",
            "user_id",
            "updated_at",
            "sex",
            "age",
            "accepts_animals",
            "smoking",
            "preferable_price",
            "description",
            "is_searchable",
            "avatar",
            "first_name",
            "last_name",
            "conversation_id",
            "point",
        ]

    def get_conversation_id(self, obj):
        try:
            user = self.context.get("request").user
            conversation = Conversation.objects.filter(users=str(obj.user_id)).filter(
                users=user.id
            )
            if conversation:
                return conversation[0].id
        except:
            return None

    def get_point(self, obj):
        return PointSerializer(
            Point.objects.all().filter(user=obj.user), many=True
        ).data

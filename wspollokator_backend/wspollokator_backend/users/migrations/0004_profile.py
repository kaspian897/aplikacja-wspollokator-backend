# Generated by Django 3.2.9 on 2021-11-14 14:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid
import wspollokator_backend.users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20211103_2240'),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('updated_at', models.DateTimeField(auto_now_add=True)),
                ('sex', models.CharField(choices=[('M', 'man'), ('F', 'female')], max_length=1, verbose_name='sex')),
                ('age', models.PositiveIntegerField(verbose_name='age')),
                ('accepts_animals', models.CharField(choices=[('A', 'accept'), ('N', 'not accept'), ('I', 'irrelevant')], max_length=1, verbose_name='accepts_animals')),
                ('smoking', models.CharField(choices=[('A', 'accept'), ('N', 'not accept'), ('I', 'irrelevant')], max_length=1, verbose_name='smoking')),
                ('preferable_price', models.DecimalField(decimal_places=2, max_digits=12, verbose_name='preferable_price')),
                ('description', models.TextField(verbose_name='description')),
                ('is_searchable', models.BooleanField(verbose_name='is_searchable')),
                ('avatar', models.ImageField(upload_to=wspollokator_backend.users.models.avatarFileName, verbose_name='avatar')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
        ),
    ]

from django.db import models
from django.contrib.auth import get_user_model
import uuid

User = get_user_model()


class Conversation(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    name = models.TextField(default="")
    users = models.ManyToManyField(User)
    is_group = models.BooleanField(default=False)
    #admins=models.ManyToManyField(User, on_delete=models.DO_NOTHING,related_name='+',null=True)
    admins=models.ManyToManyField(User,related_name='admin_conversation')
	

class Message(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, unique=True, editable=False
    )
    created_at = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    conversation = models.ForeignKey(Conversation, on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.text

    class Meta:
        ordering = ("created_at",)

from django.contrib import admin

from wspollokator_backend.conversation.models import Conversation, Message

admin.site.register(Conversation)
admin.site.register(Message)
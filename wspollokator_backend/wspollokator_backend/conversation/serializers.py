from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from rest_framework import serializers
from wspollokator_backend.conversation.models import (
    Conversation,
    Message,
)
from wspollokator_backend.users.serializers import (
    UserNamesSerialiazer,
    SignupSerializer,
    ProfileSerializer,
)

User = get_user_model()

class NewGroupConversationSerializer(serializers.Serializer):
    user_ids = serializers.ListField(child=serializers.UUIDField()) 
    name = serializers.CharField()

class NewConversationSerializer(serializers.Serializer):
    user_id = serializers.UUIDField()

class NewConversationSuccessSerializer(serializers.Serializer):
    id = serializers.UUIDField()
    information = serializers.CharField()


class ConversationSerializer(serializers.Serializer):
    messages = serializers.RelatedField(many=True, read_only=True)

    class Meta:
        fields = Conversation
        fields = ["messages"]


class MessageSerializer(serializers.ModelSerializer):

    user_name = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ["user", "user_name", "text", "is_read", "created_at"]

    def get_user_name(self, obj):
        return f"{obj.user.first_name} {obj.user.last_name}"

    def get_user(self, obj):
        return obj.user.id


class ConversationUserSerializer(serializers.ModelSerializer):
    avatar = serializers.FileField(
        source="profile.avatar", required=False, allow_null=True
    )

    class Meta:
        model = get_user_model()
        fields = ["id", "first_name", "last_name", "profile", "avatar"]


class ConversationListSerializer(serializers.ModelSerializer):
    last_message = serializers.SerializerMethodField("get_last_message")
    users = ConversationUserSerializer(many=True, read_only=True)
    class Meta:
        model = Conversation
        fields = ["id", "users", "last_message","admins","name","is_group"]

    def get_last_message(self, obj):
        return MessageSerializer(Message.objects.filter(conversation=obj).last()).data


class TextSerializer(serializers.Serializer):
    text = serializers.CharField()

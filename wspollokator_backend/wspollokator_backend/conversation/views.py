from rest_framework import mixins, viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from wspollokator_backend.conversation.serializers import (
    ConversationListSerializer,
    MessageSerializer,
    NewConversationSerializer,
    NewConversationSuccessSerializer,
    TextSerializer,
    NewGroupConversationSerializer,
)
from rest_framework import serializers
from wspollokator_backend.conversation.utils import is_valid_uuid
from .models import Conversation, Message
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action

User = get_user_model()


class ConversationViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    queryset = Conversation.objects.all()
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.Serializer

    @swagger_auto_schema(
        request_body=NewConversationSerializer(),
        responses={200: NewConversationSuccessSerializer()},
    )
    def create(self, request, *args, **kwargs):
        """Creates new conversation object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        if str(user.id) == str(request.data["user_id"]):
            return Response(
                {"information": "Bad ID."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        user2 = get_object_or_404(User, id=request.data["user_id"])
        conversation = Conversation()
        if Conversation.objects.filter(users=str(user.id)).filter(
            users=request.data["user_id"]
        ):
            return Response(
                {"information": "This conversation already exists."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        conversation.save()
        conversation.users.add(user)
        conversation.users.add(user2)
        conversation.admin=user
        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "id": conversation.id,
                "information": "New conversation has been created.",
            },
            status=status.HTTP_201_CREATED,
            headers=headers,
        )


    @action(
        detail=False,
        url_path="group",
        url_name="group",
        methods=["POST"],
    )
    @swagger_auto_schema(
        request_body=NewGroupConversationSerializer(),
        responses={200: NewConversationSuccessSerializer()},
    )
    def create_group(self, request, *args, **kwargs):
        """Creates new conversation object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        admin = request.user
        #print("aaaaa",serializer)
        if (not request.data.get("user_ids")):
            return Response(
                {"information": "Bad IDs."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if (not request.data.get("name")):
            return Response(
                {"information": "Nie podano nazwy grupy"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        dict_data=request.data
        list_users=request.data["user_ids"]
        list_users.append(admin.id)
        list_users=list(set(list_users))
		
        conversation = Conversation()
        conversation.save()
        for user in list_users:
            print(user)
            conversation.users.add(get_object_or_404(User, id=user))
        conversation.is_group=True
        conversation.admins.add(admin)
        conversation.name=request.data["name"]
		
        conversation.save()
        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "id": conversation.id,
                "information": "New conversation has been created.",
            },
            status=status.HTTP_201_CREATED,
            headers=headers,
        )
    @action(
        detail=True,
        url_path="name",
        url_name="name",
        methods=["POST"],
    )
    @swagger_auto_schema(
        request_body=NewGroupConversationSerializer(),
        responses={200: NewConversationSuccessSerializer()},
    )
    def update_name(self, request,pk=None, *args, **kwargs):
        """Creates new conversation object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user

        if (not request.data.get("name")):
            return Response(
                {"information": "Nie podano nazwy grupy"},
                status=status.HTTP_400_BAD_REQUEST,
            )
		
        conversation = Conversation.objects.filter(id=pk,is_group=True,admins=user).first()
        if(conversation==None):
            return Response(
                {
                    "information": "Nie masz uprawnień by to zrobić",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        conversation.name=request.data["name"]
		
        conversation.save()
        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "id": conversation.id,
                "information": "Pomyślnie zmieniono nazwę",
            },
            status=status.HTTP_200_OK,
            headers=headers,
        )

    @action(
        detail=True,
        url_path="user/remove",
        url_name="group_remove_users",
        methods=["POST"],
        
    )
    @swagger_auto_schema(
        request_body=NewGroupConversationSerializer(),
        responses={200: NewConversationSuccessSerializer()},
    )
    def remove_user(self, request,pk=None, *args, **kwargs):
        """Creates new conversation object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        admin = request.user

        if (not request.data.get("user_ids")):
            return Response(
                {"information": "Bad IDs."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        
        dict_data=request.data
        list_users=request.data["user_ids"]
        list_users=list(set(list_users))
        conversation = Conversation.objects.filter(id=pk,is_group=True,admins=admin).first()

        if(conversation==None):
            return Response(
                {
                    "information": "Nie znaleziono konwersacji",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
			
			
        has_admins=conversation.admins.filter(id__in=list_users).first()
        if(not has_admins==None):
            return Response(
                {
                    "information": "Nie masz uprawnień by to zrobić",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        for user in list_users:
            u=get_object_or_404(User, id=user)
            if(not u==admin.id):
                conversation.users.remove(u)
		
        #conversation.save()
        headers = self.get_success_headers(serializer.data)
        return Response(
            {
                "id": conversation.id,
                "information": "Pomyślnie usunięto użytkowników",
            },
            status=status.HTTP_200_OK,
            headers=headers,
        )
		
		
    @action(
        detail=True,
        url_path="leave",
        url_name="leave_group",
        methods=["POST"],
        
    )
    #@swagger_auto_schema(
    #    request_body=NewGroupConversationSerializer(),
    #    responses={200: NewConversationSuccessSerializer()},
    #)
    def leave(self, request,pk=None, *args, **kwargs):
        """Creates new conversation object."""
        user = request.user

        conversation = Conversation.objects.filter(id=pk).first()
        if(conversation==None):
            return Response(
                {
                    "information": "Nie ma takiej konwersacji",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        is_admin=conversation.admins.filter(id=user.id).first()
        admin_count=conversation.admins.all().count()
        if(is_admin==None):
            conversation.users.remove(user)
        else:
            conversation.admins.remove(user)
            conversation.users.remove(user)
            if(admin_count<=1):
                conversation.admins.set(conversation.users.all())
		
        conversation.save()
        return Response(
            {
                "information": "Opuszczono konwersacje",
            },
            status=status.HTTP_200_OK,
        )
		
		
		
    @action(
        detail=True,
        url_path="add",
        url_name="add_user",
        methods=["POST"],
        
    )
    @swagger_auto_schema(
        request_body=NewGroupConversationSerializer(),
    )
    def add_user(self, request,pk=None, *args, **kwargs):
        """Creates new conversation object."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        #print("aaaaa",serializer)
        if (not request.data.get("user_ids")):
            return Response(
                {"information": "Bad IDs."},
                status=status.HTTP_400_BAD_REQUEST,
            )


        conversation = Conversation.objects.filter(id=pk).first()
        if(conversation==None):
            return Response(
                {
                    "information": "Nie znaleziono konwersacji",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        is_admin=conversation.admins.filter(id=user.id).first()
        admin_count=conversation.admins.all().count()
        if(is_admin==None and  conversation.is_group):
            return Response(
                {
                    "information": "Nie masz uprawnień by to zrobić",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        if(conversation.is_group==False):
            conversation.admins.set(conversation.users.all())
            conversation.is_group=True
            conversation.name="Nowa grupa"
			
			
        dict_data=request.data
        list_users=request.data["user_ids"]
        list_users=list(set(list_users))
        for user_id in list_users:
            u=get_object_or_404(User, id=user_id)
            if(not u==user.id):
                conversation.users.add(u)
				

        conversation.save()
        return Response(
            {
                "id": conversation.id,
                "information": "Pomyślnie dodano użytkowników do konwersacji",
            },
            status=status.HTTP_200_OK,
        )
		
    @swagger_auto_schema(
        responses={200: MessageSerializer(many=True)},
    )
    def retrieve(self, request, pk=None):
        """Retrieves single conversation."""
        if not is_valid_uuid(pk):
            return Response(
                {"information": "Bad Id format."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        queryset = Conversation.objects.all()
        conversation = get_object_or_404(queryset, pk=pk)
        if not request.user in conversation.users.all():
            raise PermissionDenied()
        users = conversation.users.all()
        users = users.exclude(id=request.user.id)
        user = users[0]
        seen_messages = Message.objects.filter(is_read=False, user=user)
        for message in seen_messages:
            message.is_read = True
            message.save()
        messages = Message.objects.filter(conversation=conversation)
        response = MessageSerializer(data=messages, many=True)
        response.is_valid()
        return Response(response.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        responses={200: ConversationListSerializer(many=True)},
    )
    def list(self, request):
        """Lists of all of the users conversations."""
        queryset = Conversation.objects.filter(users=request.user.id)
        print(queryset.query)
        # response = []
        serializer = ConversationListSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        detail=True,
        url_path="message",
        url_name="message",
        methods=["POST"],
    )
    def message(self, request, pk=None):
        """Posts new message to a conversation."""
        if not is_valid_uuid(pk):
            return Response(
                {"information": "Bad Id format."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        queryset = Conversation.objects.all()
        conversation = get_object_or_404(queryset, pk=pk)
        if not request.user in conversation.users.all():
            raise PermissionDenied()
        serializer = TextSerializer(data=request.data)
        serializer.is_valid()
        message = Message(
            conversation=conversation, user=request.user, text=request.data["text"]
        )
        message.save()
        return Response("Message has been sent.", status=status.HTTP_200_OK)

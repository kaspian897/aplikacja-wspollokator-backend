# wspollokator_backend

[![Build Status](https://travis-ci.org/WojdaLukasz/wspollokator_backend.svg?branch=master)](https://travis-ci.org/WojdaLukasz/wspollokator_backend)
[![Built with](https://img.shields.io/badge/Built_with-Cookiecutter_Django_Rest-F7B633.svg)](https://github.com/agconti/cookiecutter-django-rest)

Wspollokator. Check out the project's [documentation](http://WojdaLukasz.github.io/wspollokator_backend/).

# Prerequisites

- [Docker](https://docs.docker.com/docker-for-mac/install/)  

# Local Development

Start the dev server for local development:
```bash
docker-compose up
```

Run a command inside the docker container:

```bash
docker-compose run --rm web [command]
```

# API DOCS
[Docs](http://127.0.0.1:8000/swagger/) 
